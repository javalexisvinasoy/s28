// console.log("MongoDB - CRUD Operations")

// MongoDB - CRUD Operations

/*
	1. Create - insert
	2. Read - find
	3. Update - update
	4. Destroy - delete
*/

// Insert Method () - create documents in our DB.
/*
	Syntax:
		Insert One Document:
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			});

		Insert Many Documents:
			db.collectionName.insertMany([
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				},

				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				}
			]);	
*/

// IMPORTANT NOTE: Upon execution on Robo 3T make sure not to execute your data several times as it will duplicate

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Dela Cruz",
	"age": 21,
	"email": "janedc@ymail.com",
	"company": "none"
})

db.users.insertMany([
		{
			"firstName": "Stephen",
			"lastName": "Hawkings",
			"age": 67,
			"email": "stephenhawkings@gmail.com",
			"department": "none"
		},

		{
			"firstName": "Neil",
			"lastName": "Armstrong",
			"age": 82,
			"email": "neilarmstrong@gmail.com",
			"department": "none"
		}
	]);

// MINI ACTIVITY

db.courses.insertMany([
		{
			"name": "Javascript 101",
			"price": 5000,
			"description": "Introduction to Javascript",
			"isActive": true
		},

		{
			"name": "HTML 101",
			"price": 2000,
			"description": "Introduction to HTML",
			"isActive": true
		},

		{
			"name": "CSS 101",
			"price": 2500,
			"description": "Introduction to CSS",
			"isActive": true
		}

	]);


// Find Document/ Method - Read
/*
	Syntax:
		db.collectionName.find() - this will retrieve all the documents from our database

		db.collectionName.find({"criteria": "value"}) - this will retrieve all the documents that will match our criteria.

		db.collectionName.findOne({"criteria": "value"}) - this will return the first document in our collection that matches our criteria

		db.collection.findOne({}) - this will return the first document in our collection
*/


db.users.find();

db.users.find({
	"firstName": "Jane"
});

db.users.find({
	"firstName": "Neil",
	"age": 82
});


// Update Documents/ Method - updates our documents in our collection

/*
	updateOne() - updating the first matching document on our collection

	Syntax:
		db.collectionName.updateOne({
			"criteria": "value"
		},
		{
			$set: {
				"fieldToBeUpdated": "updatedValue"
			}
		})

	
	updateMany() - multiple; it updates all the documents that matches our criteria

	Syntax:
		db.collectionName.updateMany(
			{
				"criteria": "value"
			},
			{
				$set: {
				"fieldToBeUpdated": "updatedValue"
			}
			})
*/

db.users.insertOne({
	"firstName": "Test",
	"lastName": "Test",
	"age": 0,
	"email": "test@hotmail.com",
	"department": "none"
});

// Updating One Document

db.users.updateOne(
	{
		"firstName": "Test"
	},
	{
		$set: {
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"email": "billgates@gmail.com",
			"department": "Operations",
			"status": "active"
		}
	});


// Removing a field
// unset - to remove
db.users.updateOne(
		{
			"firstName": "Bill"
		},
		{
			$unset: {
				"status": "active"
			}
		}
	)

// Updating Multiple Documents

db.users.updateMany(
		{
			"department": "none"
		},
		{
			$set: {
				"department": "HR"
			}
		}
	);

db.users.updateOne({},
	{
		$set: {
			"department": "Operations"
		}
	})


// Mini Activity 2

db.courses.updateOne(
	{
		"name": "HTML 101"
	},
	{
		$set: {
			"isActive": false
		}
	})

db.courses.updateMany(
		{},
		{
			$set: {
				"Enrollees": 10
			}
		}
	);